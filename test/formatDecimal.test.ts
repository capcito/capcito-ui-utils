import * as Pkg from '../src/index'

describe('formatDecimal', () => {
  it('should be default exported', () => {
    expect(Pkg).toHaveProperty('formatDecimal')
  })

  it('should work with no decimals', () => {
    const result = Pkg.formatDecimal(2020.293922, 0)

    expect(result).toBe(2020)
  })

  it('should work with 3 decimals', () => {
    const result = Pkg.formatDecimal(2020.293922, 3)

    expect(result).toBe(2020.294)
  })

  it('should work with default decimal value', () => {
    const result = Pkg.formatDecimal(2020.293922)

    expect(result).toBe(2020.29)
  })
})
