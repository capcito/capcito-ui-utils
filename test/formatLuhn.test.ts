import * as Pkg from '../src/index'

describe('formatLuhn', () => {
  it('should be default exported', () => {
    expect(Pkg).toHaveProperty('formatLuhn')
  })

  it('should work with 10 characters', () => {
    const result = Pkg.formatLuhn('5590015185')

    expect(result).toBe('559001-5185')
  })

  it('should work with 12 characters', () => {
    const result = Pkg.formatLuhn('199001011234')

    expect(result).toBe('19900101-1234')
  })
})
