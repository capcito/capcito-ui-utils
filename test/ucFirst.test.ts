import * as Pkg from '../src/index'

describe('ucFirst', () => {
  it('should be default exported', () => {
    expect(Pkg).toHaveProperty('ucFirst')
  })

  it('should work with all lowercase', () => {
    expect(Pkg.ucFirst('test')).toBe('Test')
  })

  it('should work with all uppercase', () => {
    expect(Pkg.ucFirst('TEST')).toBe('Test')
  })

  it('should work with swedish characters', () => {
    expect(Pkg.ucFirst('å')).toBe('Å')
    expect(Pkg.ucFirst('ä')).toBe('Ä')
    expect(Pkg.ucFirst('ö')).toBe('Ö')
  })
})
