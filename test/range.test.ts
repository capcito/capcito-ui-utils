import * as Pkg from '../src/index'

describe('range', () => {
  it('should be default exported', () => {
    expect(Pkg).toHaveProperty('range')
  })

  it('should work with no iterations', () => {
    expect(Pkg.range(1000, 1000, 1000))
      .toStrictEqual([1000])
  })

  it('should work with multiple iterations', () => {
    expect(Pkg.range(1, 5, 1))
      .toStrictEqual([1, 2, 3, 4, 5])

    expect(Pkg.range(1, 5, 2))
      .toStrictEqual([1, 3, 5])
  })
})
