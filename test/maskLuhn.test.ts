import * as Pkg from '../src/index'

describe('maskLuhn', () => {
  it('should be default exported', () => {
    expect(Pkg).toHaveProperty('maskLuhn')
  })

  it('should work with 10 characters', () => {
    const result = Pkg.maskLuhn('5590015185')

    expect(result).toBe('559001XXXX')
  })

  it('should work with 12 characters', () => {
    const result = Pkg.maskLuhn('199001011234')

    expect(result).toBe('19900101XXXX')
  })
})
