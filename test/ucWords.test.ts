import * as Pkg from '../src/index'

describe('ucFirst', () => {
  it('should be default exported', () => {
    expect(Pkg).toHaveProperty('ucWords')
  })

  it('should work with only one word', () => {
    expect(Pkg.ucWords('test')).toBe('Test')
  })

  it('should work with multiple words', () => {
    expect(Pkg.ucWords('this is a test')).toBe('This Is A Test')
  })

  it('should work with swedish characters', () => {
    expect(Pkg.ucWords('å ä ö')).toBe('Å Ä Ö')
  })
})
