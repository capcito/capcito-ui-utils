/**
 * Take any string, transform the first letter in the string with a uppercase version of that letter.
 *
 * Output: test => Test
 */
export default function (input: string): string {
  return `${input.charAt(0).toUpperCase()}${input.slice(1).toLowerCase()}`
}
