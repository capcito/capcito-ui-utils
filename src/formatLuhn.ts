/**
 * Accept given input as a string.
 *
 * Returns an formatted version.
 *
 * For example YYYYMMDDXXXX returns as YYYYMMDD-XXXX
 */
export default function (input: string) {
  if (input.length !== 10 && input.length !== 12) {
    throw new Error('The given input length is invalid, 10 or 12 characters is required')
  }

  const start = input.substr(0, input.length - 4)
  const end = input.slice(-4)

  return `${start}-${end}`
}
