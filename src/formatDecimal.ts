/**
 * Accept given input as a numeric number.
 *
 * Returns the numeric valus as a float with the given amount of decimals.
 */
export default function (input: number, decimals: number = 2) {
  return parseFloat(input.toFixed(decimals))
}
