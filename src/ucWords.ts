import { ucFirst } from './index'

/**
 * Take any string, transform the first letter in each word with the uppercase version of that letter.
 *
 * Output: test me => Test Me
 */
export default function (input: string): string {
  return input.split(' ').map(w => ucFirst(w)).join(' ')
}
