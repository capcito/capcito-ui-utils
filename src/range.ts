/**
 * Accept a start, end an incremental value.
 *
 * Given: rangeIncrement(1000, 5000, 1000)
 * Output: [1000, 2000, 3000, 4000, 5000]
 */
export default function (start: number, end: number, increment: number): number[] {
  const result: number[] = [start]
  let current = start

  while (current < end) {
    current += increment
    result.push(current)
  }

  return result
}
